#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd

def load_dataset(strukturparametername, eingabeparametername):
    pv_module = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="PV_Module")
    stromspeicher = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Stromspeicher")
    wechselrichter = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Hybrid-Wechselrichter")
    strukturparameter = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Strukturparameter")
    eingabeparameter = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Eingabeparameter")
    
    
    pv_module = pv_module.set_index(['Modell'], drop=True).to_dict(orient='index')
    stromspeicher = stromspeicher.set_index(['Modell'], drop=True).to_dict(orient='index')
    wechselrichter = wechselrichter.set_index(['Modell'], drop=True).to_dict(orient='index')
    strukturparameter = strukturparameter.set_index(['Parametergruppe'], drop=True)
    eingabeparameter = eingabeparameter.set_index(['Name'], drop=True)
    
    #%% Struktur- und Eingabeparameter auswählen
    strukturparameter = strukturparameter.loc[strukturparametername].to_dict()
    eingabeparameter = eingabeparameter.loc[eingabeparametername].to_dict()
    
    #%%

    
    data = {
            None:{
                "Panels" : {None : list(pv_module)},
                "Speicher" : {None : list(stromspeicher)},
                "Wechselrichter" : {None : list(wechselrichter)},
                "panelkosten" : {panelname : pv_module[panelname]
                                 ["Preis pro Modul [€]"] for panelname in pv_module},
                "panelleistung" : {panelname : pv_module[panelname]
                                   ["Maximalleistung [kWp]"] for panelname in pv_module},
                "panellaenge" : {panelname : pv_module[panelname]
                                 ["Länge [m]"] for panelname in pv_module},
                "panelbreite" : {panelname : pv_module[panelname]
                                 ["Breite [m]"] for panelname in pv_module}
                  }
            }
    
    params = {"Strukturparameter": strukturparameter,
              "Eingabeparameter" : eingabeparameter
              }
    
    return data, params
